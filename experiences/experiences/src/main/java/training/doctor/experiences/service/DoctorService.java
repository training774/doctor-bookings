package training.doctor.experiences.service;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.SuppressAjWarnings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;
import training.doctor.experiences.Repo.DoctorInterface;
import training.doctor.experiences.enums.CountriesEnums;
import training.doctor.experiences.enums.TimeEnum;
import training.doctor.experiences.model.request.AppointmentRequest;
import training.doctor.experiences.model.request.DocterAvaliablityrequest;
import training.doctor.experiences.model.response.ResponseModel;

import java.time.LocalDate;

@Slf4j
@Service
public class DoctorService {
  @Autowired DoctorInterface service;

  public ResponseModel getAvailabilty(
      String doctorName,
      String clinicName,
      String clinicCity,
      LocalDate date,
      TimeEnum appointmentTime,
      String uuid) {
    ResponseModel response =
        service.checkDoctorAvailablity(
            doctorName, clinicName, clinicCity, date, appointmentTime, uuid);
    if (response.getStatus().equals("400") || response.getStatus().equals("500")) {
      throw new RuntimeException(response.getError());
    }
    return response;
  }

  public ResponseModel makeBooking(
      AppointmentRequest request, CountriesEnums countriesEnums, String uuid) {
    ResponseModel reponse = service.bookAppointment(request, countriesEnums, uuid);
    if (reponse.getStatus().equals("400") || reponse.getStatus().equals("500")) {
      throw new RuntimeException(reponse.getError());
    }
    return reponse;
  }

  public ResponseModel updateBooking(AppointmentRequest request, String id, String uuid) {
    ResponseModel reponse = service.updateBooking(request, id, uuid);
    if (reponse.getStatus().equals("400") || reponse.getStatus().equals("500")) {
      throw new RuntimeException(reponse.getError());
    }
    return reponse;
  }

  public ResponseModel getAllClinincs(String uuid) {
    return service.getAllClinics(uuid);
  }
}
