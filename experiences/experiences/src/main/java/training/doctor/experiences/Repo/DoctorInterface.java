package training.doctor.experiences.Repo;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import training.doctor.experiences.enums.CountriesEnums;
import training.doctor.experiences.enums.TimeEnum;
import training.doctor.experiences.model.request.AppointmentRequest;
import training.doctor.experiences.model.request.ClinicDoctorRequest;
import training.doctor.experiences.model.request.ClinicDoctorUpdateRequest;
import training.doctor.experiences.model.request.DocterAvaliablityrequest;
import training.doctor.experiences.model.response.ResponseModel;

import java.time.LocalDate;

@FeignClient(name = "doctor", url = "${config.rest.service.bookingUrl}")
public interface DoctorInterface {
    @GetMapping("/doctor-avaliablity")
    public ResponseModel checkDoctorAvailablity(@RequestParam("doctorName") String doctorName , @RequestParam("clinicName") String clinicName , @RequestParam("clinicCity") String clinicCity , @RequestParam("date")  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date, @RequestParam("appointmentTime") TimeEnum appointmentTime,@RequestParam("uuid") String uuid);
    @PostMapping("/booking")
    public ResponseModel bookAppointment (@RequestBody AppointmentRequest request, @RequestHeader("country") CountriesEnums countriesEnums, @RequestHeader("uuid") String uuid);

    @PutMapping("/update-booking")
    public ResponseModel updateBooking(@RequestBody AppointmentRequest request, @RequestParam("id") String id,@RequestHeader("uuid") String uuid);

    @DeleteMapping("/delete-booking")
    public ResponseModel deleteBooking(@RequestParam("id") String id);

    @PostMapping("/doctors")
    public ResponseModel createNewDoctor(@RequestBody ClinicDoctorRequest request, @RequestHeader("country") CountriesEnums countriesEnums);

    @PutMapping("/doctors")
    public ResponseModel updateDocter (@RequestBody ClinicDoctorUpdateRequest request, @RequestHeader("country") CountriesEnums countriesEnums);
    @GetMapping("/doctors")
    public ResponseModel getAllClinics(@RequestHeader("uuid") String uuid);
}
