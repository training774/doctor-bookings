package training.doctor.experiences.controller;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import training.doctor.experiences.enums.CountriesEnums;
import training.doctor.experiences.enums.TimeEnum;
import training.doctor.experiences.model.request.AppointmentRequest;
import training.doctor.experiences.model.request.DocterAvaliablityrequest;
import training.doctor.experiences.model.response.ResponseModel;
import training.doctor.experiences.service.DoctorService;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.EnumMap;
import java.util.Optional;

@Slf4j
@RestController
public class DoctorController {
  @Autowired DoctorService service;

  @ApiOperation(value = "Check for Doctor Availablity")
  @GetMapping("/doctor-availablity")
  public ResponseEntity<ResponseModel> checkAvailability(
      @RequestParam("doctorName") String doctorName,
      @RequestParam("clinicName") String clinicName,
      @RequestParam("clinicCity") String clinicCity,
      @RequestParam("date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date,
      @RequestParam("time") TimeEnum appointmentTime,
      @RequestParam("uuid") String uuid) {
    try {
      return new ResponseEntity<>(
          service.getAvailabilty(doctorName, clinicName, clinicCity, date, appointmentTime, uuid),
          HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(
          new ResponseModel(Optional.ofNullable(null), e.getMessage(), "400"),
          HttpStatus.BAD_REQUEST);
    }
  }

  @ApiOperation(value = "For booking doctor appointment")
  @PostMapping("/doctor-booking")
  public ResponseEntity<ResponseModel> makeBookings(
      @RequestBody @Valid AppointmentRequest request,
      @RequestHeader("country") CountriesEnums countriesEnums,
      @RequestHeader("uuid") String uuid) {
    try {
      log.info("call made to for booking {}", uuid);
      ResponseModel response = service.makeBooking(request, countriesEnums, uuid);
      log.info("response sent back {}", uuid);
      return new ResponseEntity<>(response, HttpStatus.CREATED);
    } catch (Exception e) {
      log.error("call made to for booking {} : with error {}", uuid, e.getMessage());
      return new ResponseEntity<>(
          new ResponseModel(Optional.ofNullable(null), e.getMessage(), "400"),
          HttpStatus.BAD_REQUEST);
    }
  }

  @ApiOperation(value = "For Updateing booking")
  @PutMapping("/update-booking")
  public ResponseEntity<ResponseModel> updateBooking(
      @RequestBody @Valid AppointmentRequest request,
      @RequestParam("id") String id,
      @RequestHeader("uuid") String uuid) {
    try {
      log.info("call made to for booking update {}", uuid);
      ResponseModel response = service.updateBooking(request, id, uuid);
      log.info("Response for booking update {}", uuid);
      return new ResponseEntity<>(response, HttpStatus.CREATED);
    } catch (Exception e) {
      return new ResponseEntity<>(
          new ResponseModel(Optional.ofNullable(null), e.getMessage(), "400"),
          HttpStatus.BAD_REQUEST);
    }
  }

  @GetMapping("/doctors")
  public ResponseEntity<ResponseModel> getAllDoctors(@RequestHeader("uuid") String uuid) {
    return new ResponseEntity<>(service.getAllClinincs(uuid), HttpStatus.OK);
  }
}
