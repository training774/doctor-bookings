package training.doctor.experiences;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class ExperiencesApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExperiencesApplication.class, args);
	}

}
